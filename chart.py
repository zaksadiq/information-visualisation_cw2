#dependencies
##descartes
##numpy
##pandas (version 1.0 or later)
##shapely (interface to GEOS; version 1.7 or later)
##fiona (interface to GDAL; version 1.8 or later)
##pyproj (interface to PROJ; version 2.6.1 or later)
##packaging

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import geopandas as gpd
import numpy as np
import matplotlib.patheffects as path_effects
import random
from descartes import PolygonPatch

world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))

def get_countries( continent ):
    figwidth=8
    continent_bounds=[-200, 200,-100, 100]
    if (continent=='Africa'):
        continent_bounds=[-30,60,-45, 45]
        countries = ['Nigeria', 'Ethiopia', 'Egypt', 'Congo','Tanzania', 'South Africa',
                     'Kenya', 'Uganda', 'Algeria', 'Sudan', 'Morocco', 'Angola',
                     'Mozambique', 'Ghana', 'Madagascar', 'Cameroon', "Côte d'Ivoire",
                     'Niger', 'Burkina Faso', 'Mali', 'Malawi', 'Zambia', 'Senegal' ,'Chad',
                     'Somalia','Zimbabwe','Guinea','Rwanda','Benin','Burundi','Tunisia',
                     'Togo','Sierra Leone','Libya','Congo','Liberia','Mauritania',
                     'Eritrea','Namibia','Gambia','Botswana','Gabon','Lesotho',
                     'Guinea-Bissau','Djibouti']
        #geopandas doesn't like these countires
        countries_causing_errors=['South Sudan', 'Central African Republic', 'Mauritius',
                                  'Comoros','Cabo Verde', 'Sao Tome & Principe',
                                  'Seychelles','Equatorial Guinea', 'Eswatini']
    elif (continent=='Americas'):
        continent_bounds=[-200, 0, -60, 90]
        countries = ['Brazil','Mexico','Colombia','Argentina','Canada',
                     'Peru','Venezuela','Chile','Ecuador','Guatemala','Bolivia','Haiti'
                     ,'Cuba','Honduras','Nicaragua','Paraguay',
                     'El Salvador','Costa Rica','Panama','Uruguay','Jamaica',
                     'Trinidad and Tobago','Guyana','Suriname','Bahamas','Belize']
        countries_causing_errors=['United States', 'Dominican Republic', 'Barbados',
                                  'Saint Lucia', 'Grenada','Saint Vincent and the Grenadines',
                                  'Antigua and Barbuda','Dominica','Saint Kitts and Nevis']
    elif (continent=='Asia'):
        figwidth=12
        continent_bounds=[15, 200, -15, 100]
        countries = ['China','India','Indonesia','Pakistan','Bangladesh',
                     'Japan','Philippines','Vietnam','Turkey','Iran','Thailand',
                     'Myanmar','South Korea','Iraq','Afghanistan','Saudi Arabia',
                     'Uzbekistan','Malaysia','Yemen','Nepal','North Korea',
                     'Sri Lanka','Kazakhstan','Syria','Cambodia','Jordan',
                     'Azerbaijan','United Arab Emirates','Tajikistan','Israel',
                     'Laos','Lebanon','Kyrgyzstan','Turkmenistan',
                     'Oman','Kuwait','Georgia','Mongolia',
                     'Armenia','Qatar','Timor-Leste','Cyprus',
                     'Bhutan','Brunei']
        countries_causing_errors=['Singapore','State of Palestine','Bahrain','Maldives']
    elif (continent=='Europe'):
        figwidth=12
        continent_bounds=[-25, 125, 25, 95]
        countries = ['Russia','Germany','United Kingdom','France',
                     'Italy','Spain','Ukraine','Poland','Romania',
                     'Netherlands','Belgium','Czechia','Greece',
                     'Portugal','Sweden','Hungary','Belarus','Serbia',
                     'Switzerland','Bulgaria','Denmark','Finland','Slovakia',
                     'Norway','Ireland','Croatia','Moldova',
                     'Albania','Lithuania','North Macedonia','Slovenia','Latvia',
                     'Estonia','Montenegro','Luxembourg','Iceland']
        countries_causing_errors=['Bosnia and Herzegovina','Malta', 'Andorra',
                                  'Monaco', 'Liechtenstein']
    elif (continent=='Oceania'):
        continent_bounds=[75, 200, -75, 25]
        countries = ['Australia','Papua New Guinea','New Zealand','Vanuatu']
        countries_causing_errors=['Fiji','Solomon Islands','Micronesia','Samoa','Kiribati',
                                  'Tonga','Marshall Islands','Palau','Tuvalu','Nauru']
    else:
        return None
    return random.sample(countries, k=4), continent_bounds, figwidth

def dataframe( countries, plastic_stats ):
    countries = {"Country":countries,
             "pollution":plastic_stats} #create random list of integers

    return pd.DataFrame(countries, index=countries["Country"]) #create dataframe, using dictionary, indexed by countries

def sort( dataFrame ):
    return dataFrame.sort_values(by="pollution", ascending=False) #sort dataframe from most pollution to least

def colour_country( ax, country, color ):
    print(country)
    # getting info we need for plotting
    country_from_geo = world[world.name == country]
    country_f = country_from_geo.__geo_interface__['features']
    country_coords = {'type': country_f[0]['geometry']['type'], \
              'coordinates': country_f[0]['geometry']['coordinates']}
    country_centre = country_from_geo.centroid
    #colour in the country on the map
    ax.add_patch(PolygonPatch( country_coords, fc=color, ec="black", alpha=0.85, zorder=2 ))
    #plot the country's name
    txt = plt.text(country_centre.x, country_centre.y, s=country, ha='center')
    txt.set_path_effects([path_effects.withStroke(linewidth=2, foreground='w')])

def dym_bar( continent, countries, plastic_stats, df, filename ):
    fig, ax = plt.subplots(1,1)
    df.plot( ax=ax, kind="bar", title="Plastic pollution by Country in " + continent.capitalize(), ylabel="Pollution(kg)",xlabel="Country") #create bar chart of countries by their pollution output
    plt.tight_layout()
    fig.savefig( filename + ".png" )
    return filename+".png"

def dym_map( continent, countries, continent_bounds, figwidth, plastic_stats, filename ):
    #fig, ax = plt.subplots(1,1)
    #plot full world map, we'll crop it later
    ax = world.plot(figsize=(figwidth,7), color='lightgrey')

    #convert plastic stats to colours
    cmap = plt.cm.Reds
    norm = matplotlib.colors.Normalize(vmin=0.1, vmax=10)
    colors = [cmap(norm(value)) for value in plastic_stats]
    
    #plot countries for which we generated data
    i=0;
    for country in countries:
       colour_country(ax, country, colors[i])
       i=i+1
       
    #don't show us the axes for longitude and latitude
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    #crop the world map to focus on the chosen continent
    ax.set_xlim(continent_bounds[0], continent_bounds[1])
    ax.set_ylim(continent_bounds[2], continent_bounds[3])
    #Title
    ax.set_title("Plastic Pollution by Country in " + continent.capitalize(), fontsize=11)
    #legend/colour key
    colourKey = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    colourKey.set_array(plastic_stats)
    #colour key's label
    plt.figtext(0.91, 0.5, 'Plastic pollution per person per year (kg)', horizontalalignment='right',
                verticalalignment='center', fontsize=11, rotation=270)
    plt.colorbar(colourKey)
    #save img of figure
    #figures=[manager.canvas.figure
    #         for manager in matplotlib._pylab_helpers.Gcf.get_all_fig_managers()]
    plt.savefig(filename + '.png')
    return filename+'.png'

def get_charts( continent, filename, filename1 ):
    countries, continent_bounds, figwidth = get_countries( continent )
    #get random plastic stats
    plastic_stats = np.random.uniform(0.1,10,4)
    df = dataframe( countries, plastic_stats )
    Map = dym_map( continent, countries, continent_bounds, figwidth, plastic_stats, filename )
    Bar = dym_bar( continent, countries, plastic_stats, df, filename1 )
    data = sort( df )
    return Map, Bar, countries, plastic_stats



